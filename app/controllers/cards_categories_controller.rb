class CardsCategoriesController < ApplicationController
  before_action :set_cards_category, only: [:show, :edit, :update, :destroy]

  # GET /cards_categories
  # GET /cards_categories.json
  def index
    @cards_categories = CardsCategory.all
  end

  # GET /cards_categories/1
  # GET /cards_categories/1.json
  def show
  end

  def display_cards_categories
    @categories = CardsCategory.where(:active => 1).all.order(card_value: :desc)
  end

  # GET /cards_categories/new
  def new
    @cards_category = CardsCategory.new
  end

  # GET /cards_categories/1/edit
  def edit
  end

  # POST /cards_categories
  # POST /cards_categories.json
  def create
    @cards_category = CardsCategory.new(cards_category_params)

    respond_to do |format|
      if @cards_category.save
        format.html { redirect_to @cards_category, notice: 'Cards category was successfully created.' }
        format.json { render :show, status: :created, location: @cards_category }
      else
        format.html { render :new }
        format.json { render json: @cards_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cards_categories/1
  # PATCH/PUT /cards_categories/1.json
  def update
    respond_to do |format|
      if @cards_category.update(cards_category_params)
        format.html { redirect_to @cards_category, notice: 'Cards category was successfully updated.' }
        format.json { render :show, status: :ok, location: @cards_category }
      else
        format.html { render :edit }
        format.json { render json: @cards_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cards_categories/1
  # DELETE /cards_categories/1.json
  def destroy
    @cards_category.destroy
    respond_to do |format|
      format.html { redirect_to cards_categories_url, notice: 'Cards category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cards_category
      @cards_category = CardsCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cards_category_params
      params.require(:cards_category).permit(:card_value, :active, :order)
    end
end
