class UserWalletsController < ApplicationController
  before_action :set_user_wallet, only: [:show]

  # GET /user_wallets
  def index
    @user_wallets = UserWallet.all
  end

  # GET /user_wallets/1
  def show
  end

  # GET /user_wallets/new
  def new
    @user_wallet = UserWallet.new
  end

  # POST /user_wallets
  def create
    @user_wallet = UserWallet.new(user_wallet_params)
    @user_wallet.currency = "USD"
    @user_wallet.uuid = SecureRandom.hex(6)

    respond_to do |format|
      if @user_wallet.save
        format.html { redirect_to @user_wallet, notice: 'User wallet was successfully created.' }
        format.json { render :show, status: :created, location: @user_wallet }
      else
        format.html { render :new }
        format.json { render json: @user_wallet.errors, status: :unprocessable_entity }
      end
    end
  end

  def wallets_status

    @wallets_status = UserWallet.where(:id => params[:id].to_i).first
    if @wallets_status.status ==  0
      @wallets_status.update(:status => 1)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Enabled')
    elsif @wallets_status.status ==  1
      @wallets_status.update(:status => 0)
      redirect_to(user_wallets_path,:notice => 'Wallet was successfully Disabled')
    else
      redirect_to(user_wallets_path,:notice => 'Sorry, something went wrong')
    end

  end

  def transfer_balance   
  end

  def transfer_balancepost
    @user_from = params[:userfrom]
    @user_to = params[:userto]
    @transfer_amount = params[:transferamount]
    @message = UserWallet.checktransfer(@user_from.to_i,@user_to.to_i,@transfer_amount)
    if @message == ""
      ActiveRecord::Base.transaction do
      @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).lock('LOCK IN SHARE MODE').first
      @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).lock('LOCK IN SHARE MODE').first
      @new_balance_from = @user_wallet_from.amount - @transfer_amount.to_f
      @new_balance_to = @user_wallet_to.amount + @transfer_amount.to_f
      @user_wallet_from.update(:amount => @new_balance_from )
      @user_wallet_to.update(:amount => @new_balance_to )
      redirect_to(user_wallets_path,:notice => "Balance was successfully transferred")
      end
    else
      redirect_to(user_wallets_path,:notice => @message )
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_wallet
      @user_wallet = UserWallet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_wallet_params
      params.require(:user_wallet).permit(:currency, :amount, :user_id, :status, :uuid)
    end
end
