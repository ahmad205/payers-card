class UserWallet < ApplicationRecord
    validates_uniqueness_of :uuid
    validates_uniqueness_of :user_id

    def self.checktransfer(user_from ,user_to ,transfer_amount)

        @user_from_exist = UserWallet.where(user_id: user_from.to_i).first
        @user_to_exist = UserWallet.where(user_id: user_to.to_i).first

        @message = ""

        if @user_from_exist != nil
            if @user_from_exist.amount < transfer_amount.to_f
                @message  = @message + "You don't have enough money for this operation"
            end

            if @user_from_exist.status == 0
                @message  = @message + "Sorry, Your Wallet was Disabled"
            end
        else
            @message  = @message + "The sender User Wasn't Stored in Our Database"
        end

        if @user_to_exist == nil
            @message  = @message + "The receiver User Wasn't Stored in Our Database"
        end

        if @user_to_exist.status == 0
            @message  = @message + "Sorry, The Receiver User Wallet was Disabled"
        end

        if user_from == user_to
            @message  = @message + "You can't transfer balance to yourself"
        end

        return   @message 
    end
end
