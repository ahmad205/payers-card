class Card < ApplicationRecord

    validates_uniqueness_of :number
    validates_length_of :number, is: 16,  message: "must be 16 digit long"
    validate :check_value
    validate :check_expiration

    def check_value
      errors.add(:value, "must be equal or greater than 5 USD") if value < 5
    end

    def check_expiration
        errors.add(:expired_at, "Must be at least two days from now") if expired_at < Date.today + 2.days
    end

end
