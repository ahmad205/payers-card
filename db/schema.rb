# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_02_132416) do

  create_table "cards", force: :cascade do |t|
    t.integer "number"
    t.integer "value"
    t.integer "status"
    t.date "expired_at"
    t.integer "user_id"
    t.integer "invoice_id", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cards_categories", force: :cascade do |t|
    t.integer "card_value"
    t.integer "active"
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "money_ops", force: :cascade do |t|
    t.string "opid"
    t.integer "optype"
    t.string "amount"
    t.integer "payment_gateway"
    t.integer "status"
    t.datetime "payment_date"
    t.integer "payment_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_wallets", force: :cascade do |t|
    t.string "currency"
    t.float "amount"
    t.integer "user_id"
    t.integer "status"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
