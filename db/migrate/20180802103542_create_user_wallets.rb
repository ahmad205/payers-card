class CreateUserWallets < ActiveRecord::Migration[5.2]
  def change
    create_table :user_wallets do |t|
      t.string :currency
      t.string :amount
      t.integer :user_id
      t.integer :status
      t.string :uuid

      t.timestamps
    end
  end
end
