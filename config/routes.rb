Rails.application.routes.draw do
  
  resources :user_wallets
  resources :money_ops
  resources :cards_categories
  resources :cards
  get "search" => "cards#search"
  post "search" => "cards#searchpost"
  get "buy_card" => "cards#buy_card" 
  get "cards_status" => "cards#cards_status"
  get "display_cards_categories" => "cards_categories#display_cards_categories"
  get "operations_status" => "money_ops#operations_status" 
  get "wallets_status" => "user_wallets#wallets_status"
  get "transfer_balance" => "user_wallets#transfer_balance"
  post "transfer_balance" => "user_wallets#transfer_balancepost"


  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :cards
      get "search" => "cards#search"
      post "search" => "cards#searchpost"
      get "buy_card" => "cards#buy_card"
      get "cards_status" => "cards#cards_status"
      resources :cards_categories
      get "display_cards_categories" => "cards_categories#display_cards_categories"
      resources :money_ops
      get "operations_status" => "money_ops#operations_status"
      resources :user_wallets
      get "wallets_status" => "user_wallets#wallets_status"
      get "transfer_balance" => "user_wallets#transfer_balance"
      post "transfer_balance" => "user_wallets#transfer_balancepost"
    end
  end
  
end
