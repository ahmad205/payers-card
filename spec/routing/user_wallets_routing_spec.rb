require "rails_helper"

RSpec.describe UserWalletsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/user_wallets").to route_to("user_wallets#index")
    end

    it "routes to #new" do
      expect(:get => "/user_wallets/new").to route_to("user_wallets#new")
    end

    it "routes to #show" do
      expect(:get => "/user_wallets/1").to route_to("user_wallets#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/user_wallets/1/edit").to route_to("user_wallets#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/user_wallets").to route_to("user_wallets#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/user_wallets/1").to route_to("user_wallets#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/user_wallets/1").to route_to("user_wallets#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/user_wallets/1").to route_to("user_wallets#destroy", :id => "1")
    end

  end
end
