require 'rails_helper'

RSpec.describe "user_wallets/show", type: :view do
  before(:each) do
    @user_wallet = assign(:user_wallet, UserWallet.create!(
      :currency => "Currency",
      :amount => "Amount",
      :user_id => 2,
      :status => 3,
      :uuid => "Uuid"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Currency/)
    expect(rendered).to match(/Amount/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Uuid/)
  end
end
