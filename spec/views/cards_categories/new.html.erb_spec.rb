require 'rails_helper'

RSpec.describe "cards_categories/new", type: :view do
  before(:each) do
    assign(:cards_category, CardsCategory.new(
      :card_value => 1,
      :active => 1,
      :order => 1
    ))
  end

  it "renders new cards_category form" do
    render

    assert_select "form[action=?][method=?]", cards_categories_path, "post" do

      assert_select "input[name=?]", "cards_category[card_value]"

      assert_select "input[name=?]", "cards_category[active]"

      assert_select "input[name=?]", "cards_category[order]"
    end
  end
end
